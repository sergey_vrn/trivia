import Foundation

extension String {
    var base64decoded: String? {
        guard let myData = Data(base64Encoded: self),
            let myDecodedString = String(data: myData, encoding: .utf8)
            else {
                return nil
        }
        return myDecodedString
    }
}

extension Array where Element == String {
    var base64decoded: [String]? {
        return try? self.map { (encodedString) -> String in
            guard let myDecodedString = encodedString.base64decoded else {
                throw NSError(domain: "decoding", code: 0, userInfo: nil)
            }
            return myDecodedString
        }
    }
}

struct Question {
    let text: String
    let correctAnswer: String
    let incorrectAnswers: [String]
}

extension Question {
    init() {
        self.init(
            text: "To be, or not to be?",
            correctAnswer: "That is the question",
            incorrectAnswers: ["To be", "Not to be"]
        )
    }
    
    init?(data: Data?) {
        guard let myData = data,
            let myJSONObject = try? JSONSerialization.jsonObject(with: myData),
            let myResponce = myJSONObject as? [String: Any]
            else {
                return nil
        }
        
        guard myResponce["response_code"] as? Int == 0 else {
            return nil
        }
        
        guard let myResults = myResponce["results"] as? [[String: Any]],
            let myResult = myResults.first,
            let myQuestionEncoded = myResult["question"] as? String,
            let myQuestion = myQuestionEncoded.base64decoded,
            let myCorrectAnswerEncoded = myResult["correct_answer"] as? String,
            let myCorrectAnswer = myCorrectAnswerEncoded.base64decoded,
            let myIncorrectAnswersEncoded = myResult["incorrect_answers"] as? [String],
            let myIncorrectAnswers = myIncorrectAnswersEncoded.base64decoded
            else {
                return nil
        }
        
        self.init(text: myQuestion, correctAnswer: myCorrectAnswer, incorrectAnswers: myIncorrectAnswers)
    }
}
