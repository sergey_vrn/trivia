import UIKit

class CategorySelectionViewController: UIViewController {
    @IBOutlet weak var selectComputersButton: UIButton!
    @IBAction func onSelectComputers(_ sender: Any) {
        selectQuestionFromCategory(18)
    }
    @IBOutlet weak var selectMathematicsButton: UIButton!
    @IBAction func onSelectMathematics(_ sender: Any) {
        selectQuestionFromCategory(19)
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var question = Question()
}

extension CategorySelectionViewController {
    
    func selectQuestionFromCategory(_ category: Int) {
        selectComputersButton.isEnabled = false
        selectMathematicsButton.isEnabled = false
        activityIndicator.startAnimating()
        
        guard let myURL = URL(string: "https://opentdb.com/api.php?amount=1&type=multiple&encode=base64&category=\(category)") else {
            showError()
            return
        }
        
        let myTask = URLSession.shared.dataTask(with: myURL) { (data, responce, error) in
            guard let myQuestion = Question(data: data) else {
                self.showError()
                return
            }
            self.question = myQuestion
            DispatchQueue.main.async { self.performSegue(withIdentifier: "ShowQuestion", sender: self) }
        }
        myTask.resume()
    }
    
    func showError()  {
        DispatchQueue.main.async {
            self.resetControls()
            let myAlert = UIAlertController(title: "Something goes wrong", message: "Please try again later", preferredStyle: .alert)
            myAlert.addAction(UIAlertAction(title: "Dismiss", style: .default))
            self.present(myAlert, animated: true)
        }
    }
    
    func resetControls() {
        selectComputersButton.isEnabled = true
        selectMathematicsButton.isEnabled = true
        activityIndicator.stopAnimating()
    }
    
}

extension CategorySelectionViewController { // MARK: Life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        resetControls()
    }
    
}

extension CategorySelectionViewController { // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let myController = segue.destination as? QuestionViewController {
            myController.question = question
        }
    }
    
}
