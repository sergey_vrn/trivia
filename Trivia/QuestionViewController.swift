import UIKit

extension MutableCollection {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            // Change `Int` in the next line to `IndexDistance` in < Swift 4.1
            let d: Int = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}

class QuestionViewController: UIViewController {
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var optionsLabel: UILabel!
    @IBOutlet weak var showAnserButton: UIButton!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    var question = Question()
    
    @IBAction func onShowAnswer(_ sender: Any) {
        optionsLabel.isHidden = true
        showAnserButton.isHidden = true
        answerLabel.isHidden = false
        doneButton.isHidden = false
    }
    
    @IBAction func onDone(_ sender: Any) {
        dismiss(animated: true)
    }
}

extension QuestionViewController { //MARK: life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionLabel.text = question.text
        
        let myIndent = "    "
        var myOptions = question.incorrectAnswers
        myOptions.append(question.correctAnswer)
        myOptions.shuffle()
        optionsLabel.text = """
        Answers:
        \(myIndent)\(myOptions.joined(separator: "\n\(myIndent)"))
        """
        
        answerLabel.text = """
        Correct answer:
        \(myIndent)\(question.correctAnswer)
        
        Incorrect answers:
        \(myIndent)\(question.incorrectAnswers.joined(separator: "\n\(myIndent)"))
        """
        
        optionsLabel.isHidden = false
        showAnserButton.isHidden = false
        answerLabel.isHidden = true
        doneButton.isHidden = true
    }
    
}
